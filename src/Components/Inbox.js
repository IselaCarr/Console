import Axios from 'axios'
import React from 'react'
import Data from './Data'
import './inbox.css'
import { Link } from 'react-router-dom'

export default class Inbox extends React.Component {
  state = {
    HEALTH_URL: 'http://200.94.146.190:5000/' + `salud`,
    SECURITY_URL: 'http://200.94.146.190:5000/' + `seguridad`,
    events: [],
    healthEvents: [],
    securityEvents: [],
    ubication: [],
    status: []
  }

  componentDidMount () {
    this.getHealthEvent()
    this.getSecurityEvent()
  }

  getHealthEvent = () => {
    Axios.get(this.state.HEALTH_URL).then(res => {
      console.log('health', res.data)
      //add new health event in events and healthEvents if this is the first event
      if (this.state.healthEvents.length == 0) {
        const healthEvents = [...this.state.healthEvents, res.data]
        const events = [...this.state.events]
        events.push(res.data)
        this.setState({
          healthEvents,
          events
        })
        // when the new event is not equal to the last event, then i can say it is completely new and i will proceede to store it
      } else if (
        JSON.stringify(
          this.state.healthEvents[this.state.healthEvents.length - 1]
        ) !== JSON.stringify(res.data)
      ) {
        const healthEvents = [...this.state.healthEvents, res.data]
        const events = [...this.state.events]
        events.push(res.data)
        this.setState({
          healthEvents,
          events
        })
      }
      console.log(this.state.events)
    })
  }

  getSecurityEvent = () => {
    Axios.get(this.state.SECURITY_URL).then(res => {
      if (res.data.estatus !== 'True') {
        return
      }
      //add new security event in events and securityEvents if this is the first event
      if (this.state.securityEvents.length == 0) {
        const securityEvents = [...this.state.securityEvents, res.data]
        const events = [...this.state.events]
        events.push(res.data)
        this.setState({
          securityEvents,
          events
        })
        // when the new event is not equal to the last event, then i can say it is completely new and i will proceede to store it
      } else if (
        JSON.stringify(
          this.state.securityEvents[this.state.securityEvents.length - 1]
        ) !== JSON.stringify(res.data)
      ) {
        const securityEvents = [...this.state.securityEvents, res.data]
        const events = [...this.state.events]
        events.push(res.data)
        this.setState({
          securityEvents,
          events
        })
      }
    })
  }

  onGetNewEventsPressed = () => {
    this.getHealthEvent()
    this.getSecurityEvent()
  }

  deleteEvent = eventIndex => {
    let events = this.state.events
    events.splice(eventIndex, 1)
    this.setState({ events })
  }

  goToMaps = () => {
    <link
      a
      hreaf='https://www.google.com.mx/maps/place/Club+Saludable/@20.7221033,-103.3812226,18z/data=!4m5!3m4!1s0x8428aff80b4d3e4d:0xe3c0d18ecfa919c1!8m2!3d20.7225113!4d-103.3817346'
    ></link>
  }

  showEvents () {
    return this.state.events.map((data, index) => {
      return (
        <div>
          <ul className='info' onDoubleClick={() => this.deleteEvent(index)}>
            <li className='infoData2'> {data.numero} </li>
            <li className='infoData2'> {data.tipo} </li>
            <li className='infoData2'> {data.fecha} </li>
            <button className='infoData3'>
              <a  target="_blank"
              href= 'https://www.google.com.mx/maps/place/Club+Saludable/@20.7221033,-103.3812226,18z/data=!4m5!3m4!1s0x8428aff80b4d3e4d:0xe3c0d18ecfa919c1!8m2!3d20.7225113!4d-103.3817346'>
              Mapa</a>
                
            </button>
            <button className='infoData3'>
              <Link to='/consola'>Consola </Link>
            </button>
            <div>
            <form className='infoData3'>
              <select id='status' name='selectStatus'>
                <option value='select'>Asignado</option>
                <option value='unselected'>No Asignado</option>
                <option value='closed'>Cerrado</option>
              </select>
            </form>
            </div>
          </ul>
          <br />
        </div>
      )
    })
  }

  render () {
    return (
      <section>
        <div className className='informationTittles'>
          <button
            className='buttonNew'
            onClick={() => this.onGetNewEventsPressed()}
          >
            Nuevo
          </button>
          <br />
          <ul className='infoTittle'>
            <li className='infoData'> Número </li>
            <li className='infoData'> Tipo de incidente </li>
            <li className='infoData'> Fecha </li>
            <li className='infoData'> Ubicación </li>
            <li className='infoData'> Consola Minerva </li>
            <li className='infoData'> Estatus </li>
          </ul>
        </div>
        <br />
        <br />
        {this.showEvents()}
      </section>
    )
  }
}
