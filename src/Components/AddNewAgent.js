import React from 'react';
import { Link } from 'react-router-dom';
import "./addNewAgent.css"

const AddNewAgent = () => {
    return (
        <form className="formAgent">
            <h2>Completa los campos con la información solicitada</h2>
            <div>
                <input type="text" placeholder="Número de agente" name="agentNumber" id="agentNumber" required /><br />
                <input type="text" placeholder="Nombre" name="name" id="name" required /><br />                
                <input type="text" placeholder="Apellido paterno" name="lastName" id="lastName" required /><br />
                <input type="text" placeholder="Apellido materno" name="mLastName" id="mLastName" required /><br />                
                <input type="text" placeholder="Teléfono" name="phoneNumber" id="phoneNumber" required /><br />
                <input type="text" placeholder="Email" name="email" id="email" required /><br />
                <input type="text" placeholder="Domicilio" name="address" id="address" required /><br />                
                <input type="text" placeholder="Tipo de agente" name="agentType" id="agentType" required /><br />
                <input type="text" placeholder="Módulo" name="module" id="module" required />
                <div>
                    <Link to='/menu'>
                        <button className='buttonAddAgent'>Añadir</button>
                    </Link>
                </div>
            </div>
        </form>
    )
}
export default AddNewAgent;