import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './logIn.css';
import image from '../Utils/minerva_Logo.png';
import Data from './Data';

class LogIn extends Component {
    state = {
        emailInput: "",
        passwordInput: "",
        email: "",
        password: "",
        arr: ""
    };

    addEmail = event => {
        const value = event.target.value
        this.setState({
            emailInput: event.target.value
        });
        localStorage.setItem("email", value)
    };

    addPassword = event => {
        const value = event.target.value
        this.setState({
            passwordInput: event.target.value
        });
        localStorage.setItem("password", value)
    };

    componentDidMount() {
        let arr = [];
        Data.data.map(element => {
            element.agents.map(doc => {
                //console.log(doc);
                arr.push(doc);
                localStorage.setItem("array", JSON.stringify(arr));
            });
        });
        this.setState({ arr })
    }

    render() {
        //console.log(localStorage);
        const { emailInput, passwordInput } = this.state;
        return (
            <div className="App">
                <div className="bg">
                    <img src={image} alt='' className="logo" /><br />
                    <label htmlFor="email" className="indications">Email</label> <br />
                    <input className="write"
                        type="text"
                        value={emailInput}
                        placeholder="example@email.com"
                        id="email"
                        onChange={this.addEmail}
                        required /><br /><br />

                    <label htmlFor="password" className="indications">Contraseña</label><br />
                    <input className="write"
                        type="password"
                        value={passwordInput}
                        placeholder="*********"
                        id="password"
                        onChange={this.addPassword}
                        required />
                    <div>
                        <Link to='/menu'>
                            <button className='buttonAccess' onClick={this.checkPassword}>Entrar</button>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}
export default LogIn;