import React, { Component } from 'react';
import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom';
import './menuRoutes.css'
import Inbox from './Inbox';
import Binnacle from './Binnacle';
import Consola from './Consola';
import AddNewAgent from './AddNewAgent';

class MenuRoutes extends Component {
    render() {
        return (
            <div>
                <Router>
                    <header className="App-header">
                        <ul className='navRouter'>
                            <li><img className='logotipo' alt="" /></li>
                            <li className='appNav'><NavLink to='/agregar_Agente'>Agregar Agente</NavLink></li>
                            <li className='appNav'><NavLink to='/historial'>Historial</NavLink></li>
                            <li className='appNav'><NavLink to='/consola'>Consola</NavLink></li>
                            <li className='appNav'><NavLink to='/bandeja_Entrada'>Bandeja de entrada</NavLink></li>
                        </ul>
                    </header>
                    <Route exact path="/bandeja_Entrada" component={Inbox} />
                    <Route path="/consola" component={Consola}/>
                    <Route path="/historial" component={Binnacle} />
                    <Route path="/agregar_Agente" component={AddNewAgent}/>
                </Router>
            </div>
        )
    }
}

export default MenuRoutes;