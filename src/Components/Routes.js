import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import LogIn from './LogIn';
import MenuRoutes from './MenuRoutes';
import { ProtectedRoute } from './ProtectedRoute';

const Routes = () => (

    <BrowserRouter>
        <Switch>
           <Route exact path='/' component={LogIn}/>
           <ProtectedRoute path='/menu' component={MenuRoutes} />
        </Switch>
    </BrowserRouter>
);

export default Routes;