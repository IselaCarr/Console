const Data = {
  data: [
    {
      passwordHR: ['QWERTY', 'ASDFGH', 'ZXCVBN', '123456'],
      agents: [
        {
          email: 'isela.carrillo@gmail.com',
          password: 'Isela'
        },
        {
          email: 'minerva@mail.com',
          password: 'minerva123'
        },
        {
          email: 'celina@nmail.com',
          password: 'celina'
        },
        {
          email: 'celina@nmail.com',
          password: 'celina'
        }
      ],
      ubication: [
        'https://www.google.com.mx/maps/place/Club+Saludable/@20.7221033,-103.3812226,18z/data=!4m5!3m4!1s0x8428aff80b4d3e4d:0xe3c0d18ecfa919c1!8m2!3d20.7225113!4d-103.3817346',
        'https://www.google.com.mx/maps/place/Jesse+Wheel/@20.6581455,-103.3366264,17.75z/data=!4m5!3m4!1s0x8428b222fa5a3095:0xa76eec777d4a7bfa!8m2!3d20.6574879!4d-103.3333636',
        'https://www.google.com.mx/maps/place/Pasteleria+y+Cursos+las+3G/@20.666439,-103.3286533,17.75z/data=!4m5!3m4!1s0x8428b227623b93cf:0x4d692cf2ed624af8!8m2!3d20.6671138!4d-103.3279355',
        'https://www.google.com.mx/maps/place/Usagui/@20.6812583,-103.3694122,16.75z/data=!4m5!3m4!1s0x8428ae17a762f34d:0x37f7430bc0977a06!8m2!3d20.6842064!4d-103.3722121',
        'https://www.google.com.mx/maps/place/C%C3%A1mara+de+Comercio+de+Guadalajara/@20.6743836,-103.4101327,16.67z/data=!4m5!3m4!1s0x8428aebf6a03c0e3:0x88a97455ea31966a!8m2!3d20.6756054!4d-103.4084353'
      ],
      status: ['No asignado', 'Asignado', 'Cerrado']
    }
  ]
}

export default Data
